package ua.com.sourceit;

import ua.com.sourceit.interfaces.Operations;
import ua.com.sourceit.model.FinancialInstitution;
import ua.com.sourceit.utils.Generator;
import ua.com.sourceit.utils.Helper;

import java.util.ArrayList;
import java.util.List;

public class FinancialInstitutionManager {

    public FinancialInstitutionManager() {
    }

    //================================================================================//

    public static List<FinancialInstitution> createFinancialInstitutionObjects(String operationName) {

        List<FinancialInstitution> financialInstitutions = new ArrayList<>();
        int numberOfFinancialInstitutions = Integer.parseInt(Helper.getProperty("number.financial.institutions"));

        switch (operationName) {

            case Operations.CURRENCY_EXCHANGE: {
/*
                List<CurrencyInstitution> currencyInstitutions = new ArrayList<>();

                // Create CurrencyInstitution objects
                for (int i = 0; i < numberOfFinancialInstitutions; i++) {
                    currencyInstitutions.add(Generator.generateBank());
                    currencyInstitutions.add(Generator.generateExchangeOffice());
                }
*/
                break;
            }
            case Operations.CREDIT: {

                // Create LoanInstitution objects
                for (int i = 0; i < numberOfFinancialInstitutions; i++) {
                    financialInstitutions.add(Generator.generateBank());
                    financialInstitutions.add(Generator.generatePawnShop());
                    financialInstitutions.add(Generator.generateCreditCafe());
                    financialInstitutions.add(Generator.generateCreditUnion());
                }

                break;
            }
            case Operations.DEPOSIT: {

                // Create DepositInstitution objects
                for (int i = 0; i < numberOfFinancialInstitutions; i++) {
                    financialInstitutions.add(Generator.generateBank());
                    financialInstitutions.add(Generator.generatePIF());
                }

                break;
            }
            case Operations.SEND_MONEY: {

                // Create Send Money Institution objects
                for (int i = 0; i < numberOfFinancialInstitutions; i++) {
                    financialInstitutions.add(Generator.generateBank());
                    financialInstitutions.add(Generator.generatePostOffice());
                }
            }
            default:
                break;
        }

        return financialInstitutions;

    }
}



