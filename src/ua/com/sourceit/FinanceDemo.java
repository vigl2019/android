package ua.com.sourceit;

import ua.com.sourceit.interfaces.Operations;
import ua.com.sourceit.model.*;
import ua.com.sourceit.model.CurrencyInstitutions.CurrencyInstitution;
import ua.com.sourceit.utils.*;

import java.util.*;
import ua.com.sourceit.interfaces.CurrencyExchange;

import static ua.com.sourceit.utils.Generator.generateSumForSendMoney;

public class FinanceDemo {

    public static void main(String[] args) {

        // For Currency Exchange
        List<CurrencyInstitution> currencyInstitutions = new ArrayList<>();
        int numberOfFinancialInstitutions = Integer.parseInt(Helper.getProperty("number.financial.institutions"));

        // Create CurrencyInstitution objects
        for (int i = 0; i < numberOfFinancialInstitutions; i++) {
            currencyInstitutions.add(Generator.generateBank());
            currencyInstitutions.add(Generator.generateExchangeOffice());
        }

        // Buy Currency
        float currencyAmount = Generator.generateCurrencyAmount();
        while (currencyAmount > 12_000) // for selling UAH via Bank
            currencyAmount -= 100;


        CurrencyBuyerAndSeller.buyCurrencyAtTheBestRate(currencyInstitutions, currencyAmount, CurrencyExchange.UAH, Generator.generateCurrenciesName());
//      CurrencyBuyerAndSeller.buyCurrencyAtTheBestRate(currencyInstitutions, currencyAmount, CurrencyExchange.UAH, CurrencyExchange.USD);

        // Sell Currency
        CurrencyBuyerAndSeller.sellCurrencyAtTheBestRate(currencyInstitutions, Generator.generateCurrencyAmount(), Generator.generateCurrenciesName(), CurrencyExchange.UAH);
//      CurrencyBuyerAndSeller.sellCurrencyAtTheBestRate(currencyInstitutions, Generator.generateCurrencyAmount(), CurrencyExchange.USD, CurrencyExchange.UAH);

        //================================================================================//

        List<FinancialInstitution> financialInstitutions = new ArrayList<>();

        // Credit

        financialInstitutions = FinancialInstitutionManager.createFinancialInstitutionObjects(Operations.CREDIT);

        // Find a Loan Institution with minimum return credit sum
        // (loan 50 000 UAH for one year)

        float sumForLoan = Generator.generateSumForLoan();
        while (sumForLoan > 4_000) // max sum for Credit Cafe
            sumForLoan -= 100;

        LoanInstitutionFinder.getReturnMinimumCreditSum(financialInstitutions, CurrencyExchange.UAH, sumForLoan, Generator.generateMonthCount());

        //================================================================================//

        // Deposit

        financialInstitutions = FinancialInstitutionManager.createFinancialInstitutionObjects(Operations.DEPOSIT);

        // Find a Deposit Institution with maximum return deposit sum
        // deposit for 12 months

         DepositInstitutionFinder.getReturnMaximumDepositSum(financialInstitutions, CurrencyExchange.UAH, Generator.generateSumForDeposit(), 12);

        //================================================================================//

        // Send Money

        financialInstitutions = FinancialInstitutionManager.createFinancialInstitutionObjects(Operations.SEND_MONEY);
        SendingMoneyFinder.getSumForSendMoney(financialInstitutions, CurrencyExchange.UAH, generateSumForSendMoney());



/*
        // Print values of currencies
        for (CurrencyInstitution currencyInstitution : currencyInstitutions) {
            Map<String, Map<String, Float>> currencies = currencyInstitution.getCurrencies();

//            float bestRate1 = currencies.get(CurrencyExchange.USD).get(CurrencyExchange.BUY);
  //          float bestRate2 = currencies.get(CurrencyExchange.EUR).get(CurrencyExchange.SELL);

            float bestRate3 = currencies.get(CurrencyExchange.USD).get(CurrencyExchange.BUY);
            float bestRate4 = currencies.get(CurrencyExchange.USD).get(CurrencyExchange.SELL);

    //        float bestRate5 = currencies.get(CurrencyExchange.EUR).get(CurrencyExchange.BUY);
      //      float bestRate6 = currencies.get(CurrencyExchange.EUR).get(CurrencyExchange.SELL);

        //    float bestRate7 = currencies.get(CurrencyExchange.EUR).get(CurrencyExchange.BUY);
          //  float bestRate8 = currencies.get(CurrencyExchange.USD).get(CurrencyExchange.SELL);

            System.out.println();
            System.out.println("//==================================================//");
            System.out.println();

    //        System.out.print("Name:" + (currencyInstitution).getName() + " " + "USD: " + bestRate1 + "; EUR: " + bestRate2);
            System.out.print("Name:" + (currencyInstitution).getName() + " " + "USD: " + bestRate3 + "; USD: " + bestRate4);
      //      System.out.print("Name:" + (currencyInstitution).getName() + " " + "EUR: " + bestRate5 + "; EUR: " + bestRate6);
        //    System.out.print("Name:" + (currencyInstitution).getName() + " " + "EUR: " + bestRate7 + "; USD: " + bestRate8);

            System.out.println();
            System.out.println("//==================================================//");
        }
*/
    }
}
