package ua.com.sourceit.interfaces;

import ua.com.sourceit.exceptions.CurrencyExchangeException;

public interface SendMoney {

    // Send Money
    float calculateMoneyForSend(String currencyName, float sum) throws CurrencyExchangeException;
}
