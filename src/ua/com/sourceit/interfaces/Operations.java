package ua.com.sourceit.interfaces;

public interface Operations {

    String CURRENCY_EXCHANGE = "Currency Exchange";
    String CREDIT = "Credit";
    String DEPOSIT = "Deposit";
    String SEND_MONEY = "Send Money";
}
