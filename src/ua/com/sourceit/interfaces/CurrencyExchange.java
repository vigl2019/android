package ua.com.sourceit.interfaces;

import ua.com.sourceit.exceptions.CurrencyExchangeException;

public interface CurrencyExchange {

    String USD = "USD";
    String EUR = "EUR";
    String UAH = "UAH";
    String RUB = "RUB";

    String BUY = "Buy";
    String SELL = "Sell";

    float buyCurrency(float currencyAmount, String currencyName) throws CurrencyExchangeException;
    float sellCurrency(float currencyAmount, String currencyTitle) throws CurrencyExchangeException;
}


