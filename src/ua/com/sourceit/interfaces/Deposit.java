package ua.com.sourceit.interfaces;

public interface Deposit {

    // Return sum after Deposit completion
    float calculateReturnDepositSum(String currencyName, float sum, int monthCount);
}
