package ua.com.sourceit.interfaces;

public interface Credit {

    // Return sum after Loan completion
    float calculateReturnCreditSum(String currencyName, float sum, int monthCount);
}
