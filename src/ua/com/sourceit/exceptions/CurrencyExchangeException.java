package ua.com.sourceit.exceptions;

public class CurrencyExchangeException extends Exception {
    public CurrencyExchangeException(String message) {
        super(message);
    }
}
