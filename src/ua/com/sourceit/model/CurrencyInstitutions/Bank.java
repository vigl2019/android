package ua.com.sourceit.model.CurrencyInstitutions;

import ua.com.sourceit.interfaces.Deposit;
import ua.com.sourceit.interfaces.SendMoney;
import ua.com.sourceit.utils.Checker;
import ua.com.sourceit.utils.Generator;
import ua.com.sourceit.utils.Helper;
import ua.com.sourceit.interfaces.CurrencyExchange;
import ua.com.sourceit.interfaces.Credit;
import ua.com.sourceit.exceptions.CurrencyExchangeException;

public class Bank extends CurrencyInstitution implements Credit, Deposit, SendMoney {

    // For Currency Exchange
    private int licenseYear = 0;
    private float commissionFixedAmount = 0;
    private float maximumExchangeAmount = 0;

    // For Credit
    private float maximumCreditCommissionPerYear = 25;
    private float maximumCreditSum = 200_000;

    // For Deposit
    private float monthDepositPercent = 0;
    private float maximumDepositSum = 0;
    private int maximumMonthForDeposit = 0;

    // For Sending Money
    private float commissionForMoneySending = 0;
    private float commissionFixedAmountForMoneySending = 0;

    //================================================================================//

    public Bank() {
    }

    public Bank(String name, String address, int licenseYear, float commissionPercent) {
        super(name, address, commissionPercent);
        super.setCurrencies(Generator.generateCurrencies(CurrencyExchange.USD, CurrencyExchange.EUR));

        this.setLicenseYear(licenseYear);
    }

    //================================================================================//

    public int getLicenseYear() {
        return licenseYear;
    }

    public void setLicenseYear(int licenseYear) {
        this.licenseYear = licenseYear;
    }

    public float getCommissionFixedAmount() {
        return commissionFixedAmount;
    }

    public void setCommissionFixedAmount(float commissionFixedAmount) {
        this.commissionFixedAmount = commissionFixedAmount;
    }

    public float getMaximumExchangeAmount() {
        return maximumExchangeAmount;
    }

    public void setMaximumExchangeAmount(float maximumExchangeAmount) {
        this.maximumExchangeAmount = maximumExchangeAmount;
    }

    public float getMaximumCreditCommissionPerYear() {
        return maximumCreditCommissionPerYear;
    }

    public void setMaximumCreditCommissionPerYear(float maximumCreditCommissionPerYear) {
        this.maximumCreditCommissionPerYear = maximumCreditCommissionPerYear;
    }

    public float getMaximumCreditSum() {
        return maximumCreditSum;
    }

    public void setMaximumCreditSum(float maximumCreditSum) {
        this.maximumCreditSum = maximumCreditSum;
    }

    public float getMonthDepositPercent() {
        return monthDepositPercent;
    }

    public void setMonthDepositPercent(float monthDepositPercent) {
        this.monthDepositPercent = monthDepositPercent;
    }

    public float getMaximumDepositSum() {
        return maximumDepositSum;
    }

    public void setMaximumDepositSum(float maximumDepositSum) {
        this.maximumDepositSum = maximumDepositSum;
    }

    public int getMaximumMonthForDeposit() {
        return maximumMonthForDeposit;
    }

    public void setMaximumMonthForDeposit(int maximumMonthForDeposit) {
        this.maximumMonthForDeposit = maximumMonthForDeposit;
    }

    public float getCommissionForMoneySending() {
        return commissionForMoneySending;
    }

    public void setCommissionForMoneySending(float commissionForMoneySending) {
        this.commissionForMoneySending = commissionForMoneySending;
    }

    public float getCommissionFixedAmountForMoneySending() {
        return commissionFixedAmountForMoneySending;
    }

    public void setCommissionFixedAmountForMoneySending(float commissionFixedAmountForMoneySending) {
        this.commissionFixedAmountForMoneySending = commissionFixedAmountForMoneySending;
    }

    //================================================================================//

    @Override
    public float buyCurrency(float currencyAmount, String currencyName) throws CurrencyExchangeException {

        Checker.checkCurrencyAmount(currencyAmount);

        float result = 0;

        if (currencyName.equals(CurrencyExchange.UAH)) {
            float maximumExchangeAmountUAH = Float.parseFloat(Helper.getProperty("maximum.exchange.amount.UAH"));

            if (currencyAmount > maximumExchangeAmountUAH)
                throw new CurrencyExchangeException("Maximum exchange amount UAH = " + maximumExchangeAmountUAH);
        }

        float rate = this.getRateForBuyingCurrency(currencyName);
        Checker.checkRate(rate);

        result = (currencyAmount - commissionFixedAmount) / rate;
        return result;
    }

    @Override
    public float sellCurrency(float currencyAmount, String currencyName) throws CurrencyExchangeException {

        Checker.checkCurrencyAmount(currencyAmount);

        float rate = this.getRateForSellingCurrency(currencyName);
        Checker.checkRate(rate);

        float result = 0;
        result = (currencyAmount - (currencyAmount * (this.getCommissionPercent()) / 100)) * rate;
        return result;
    }

    //================================================================================//

    @Override
    public float calculateReturnCreditSum(String currencyName, float sum, int monthCount) {

        // Check sum > 0 and monthCount > 0

        /*


         */

        // Check Max/Min Credit Sum

        /*


         */

        // Check Max/Min month count for Credit

        /*


         */

        float returnCreditSum = 0;

        switch(currencyName) {

            case CurrencyExchange.UAH : {

                if (sum > maximumCreditSum)
                    System.out.println("Maximum credit sum = " + maximumCreditSum);
                else
                    returnCreditSum = (sum * maximumCreditCommissionPerYear / 100 / 12) * monthCount;

                break;
            }

            case CurrencyExchange.USD : {
                returnCreditSum = (sum * maximumCreditCommissionPerYear / 100 / 12) * monthCount;
                break;
            }

            case CurrencyExchange.EUR : {
                returnCreditSum = (sum * maximumCreditCommissionPerYear / 100 / 12) * monthCount;
            }
            default:
                break;
        }

        return Helper.roundUpToTwoDecimalPlaces(returnCreditSum);
    }

    //================================================================================//

    public float calculateReturnDepositSum(String currencyName, float sum, int monthCount) {

        // Check sum > 0 and monthCount > 0

        /*


         */

        // Check Max Deposit Sum

        /*


         */

        // Check Max/Min month count for Deposit

        /*


         */

        float returnDepositSum = 0;

        switch(currencyName) {

            case CurrencyExchange.UAH : {

                if (sum > maximumDepositSum)
                    System.out.println("Maximum deposit sum = " + maximumDepositSum);
                else
                    returnDepositSum = sum * monthCount * (monthDepositPercent / 100);

                break;
            }

            case CurrencyExchange.USD : {
                returnDepositSum = sum * monthCount * (monthDepositPercent / 100);
                break;
            }

            case CurrencyExchange.EUR : {
                returnDepositSum = sum * monthCount * (monthDepositPercent / 100);
            }
            default:
                break;
        }

        return Helper.roundUpToTwoDecimalPlaces(returnDepositSum);
    }

    //================================================================================//

    @Override
    public float calculateMoneyForSend(String currencyName, float sum) throws CurrencyExchangeException {

        // Check sum
        Checker.checkCurrencyAmount(sum);

        float moneyForSend = (sum - (sum * (this.getCommissionForMoneySending() / 100))) - this.getCommissionFixedAmountForMoneySending();

        return Helper.roundUpToTwoDecimalPlaces(moneyForSend);
    }

    //================================================================================//

    @Override
    public void printInfo(String currencyName){

//      System.out.println();
//      System.out.println("Financial Institution");
        System.out.println();

        System.out.println("Name: " + this.getName());
        System.out.println("Address: " + this.getAddress());
        System.out.println("License Year: " + this.getLicenseYear());

        System.out.println();
        System.out.println("Currency: " + currencyName);
        System.out.println();

        // For Currency Exchange
        System.out.println("For Currency Exchange");
        System.out.println();
        System.out.println("Commission Fixed Amount: " + this.getCommissionFixedAmount());
        System.out.println("Maximum Exchange Amount: " + this.getMaximumExchangeAmount());


        if(!currencyName.equals(CurrencyExchange.UAH)) {
            try {
                System.out.println("Rate: " + this.getRateForBuyingCurrency(currencyName));
            } catch (CurrencyExchangeException e) {
                System.out.println(e.getMessage());
            }
        }

        // For Credit
        System.out.println();
        System.out.println("For Credit");
        System.out.println();
        System.out.println("Maximum Credit Commission per Year: " + this.getMaximumCreditCommissionPerYear());
        System.out.println("maximumCreditSum: " + this.getMaximumCreditSum());

        // For Deposit
        System.out.println();
        System.out.println("For Deposit");
        System.out.println();
        System.out.println("Month Deposit Percent: " + this.getMonthDepositPercent());
        System.out.println("Maximum Deposit Sum: " + this.getMaximumDepositSum());
        System.out.println("Maximum Month For Deposit: " + this.getMaximumMonthForDeposit());

        // For Money Sending
        System.out.println();
        System.out.println("For Money Sending");
        System.out.println();
        System.out.println("Commission for Money Sending: " + this.getCommissionForMoneySending());
        System.out.println("Commission Fixed Amount for Money Sending: " + this.getCommissionFixedAmountForMoneySending());

        System.out.println();
        System.out.println("----------");
    }
}
