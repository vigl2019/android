package ua.com.sourceit.model.CurrencyInstitutions;

import ua.com.sourceit.interfaces.CurrencyExchange;
import ua.com.sourceit.exceptions.CurrencyExchangeException;
import ua.com.sourceit.model.FinancialInstitution;

import java.util.Map;

// Base Class for Bank and ExchangeOffice
public abstract class CurrencyInstitution extends FinancialInstitution implements CurrencyExchange {

    private float commissionPercent = 0;
    private Map<String, Map<String, Float>> currencies;

    //================================================================================//

    public CurrencyInstitution() {
    }

    public CurrencyInstitution(String name, String address, float commissionPercent) {
        super(name, address);
        this.setCommissionPercent(commissionPercent);
    }

    //================================================================================//

    public float getCommissionPercent() {
        return commissionPercent;
    }

    public void setCommissionPercent(float commissionPercent) {
        this.commissionPercent = commissionPercent;
    }

    public Map<String, Map<String, Float>> getCurrencies() {
        return currencies;
    }

    public void setCurrencies(Map<String, Map<String, Float>> currencies) {
        this.currencies = currencies;
    }

    //================================================================================//

    public float getRateForBuyingCurrency(String currencyName) throws CurrencyExchangeException {

        float rateForBuyingCurrency = 0;
        Map<String, Map<String, Float>> currencies = this.getCurrencies();

        if (currencies.get(currencyName).get(CurrencyExchange.BUY) == null)
            throw new CurrencyExchangeException("The Currency: " + currencyName + " does not exsist!");
        else
            rateForBuyingCurrency = currencies.get(currencyName).get(CurrencyExchange.BUY);

        return rateForBuyingCurrency;
    }

    public float getRateForSellingCurrency(String currencyName) throws CurrencyExchangeException{

        float rateForSellingCurrency = 0;
        Map<String, Map<String, Float>> currencies = this.getCurrencies();

        if (currencies.get(currencyName).get(CurrencyExchange.SELL) == null)
            throw new CurrencyExchangeException("The Currency: " + currencyName + " does not exsist!");
        else
            rateForSellingCurrency = currencies.get(currencyName).get(CurrencyExchange.SELL);

        return rateForSellingCurrency;
    }
}


