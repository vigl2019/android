package ua.com.sourceit.model.CurrencyInstitutions;

import ua.com.sourceit.utils.Checker;
import ua.com.sourceit.utils.Generator;
import ua.com.sourceit.interfaces.CurrencyExchange;
import ua.com.sourceit.exceptions.CurrencyExchangeException;

public class ExchangeOffice extends CurrencyInstitution {

    public ExchangeOffice() {
    }

    public ExchangeOffice(String name, String address, float commissionPercent) {
        super(name, address, commissionPercent);
        super.setCurrencies(Generator.generateCurrencies(CurrencyExchange.USD, CurrencyExchange.EUR));
    }

    //================================================================================//

    @Override
    public float buyCurrency(float currencyAmount, String currencyName) throws CurrencyExchangeException {

        Checker.checkCurrencyAmount(currencyAmount);

        float rate = this.getRateForBuyingCurrency(currencyName);
        Checker.checkRate(rate);

        float result = 0;
        result = (currencyAmount - (currencyAmount * (this.getCommissionPercent()) / 100)) / rate;
        return result;
    }

    @Override
    public float sellCurrency(float currencyAmount, String currencyName) throws CurrencyExchangeException {

        Checker.checkCurrencyAmount(currencyAmount);

        float rate = this.getRateForSellingCurrency(currencyName);
        Checker.checkRate(rate);

        float result = 0;
        result = (currencyAmount - (currencyAmount * (this.getCommissionPercent()) / 100)) * rate;
        return result;
    }

    //================================================================================//

    @Override
    public void printInfo(String currencyName){

//      System.out.println();
//      System.out.println("Financial Institution");
        System.out.println();

        System.out.println("Name: " + this.getName());
        System.out.println("Address: " + this.getAddress());
        System.out.println();

        // For Currency Exchange
        System.out.println("For Currency Exchange");
        System.out.println();
        System.out.println("Currency: " + currencyName);

        try {
            System.out.println("Rate: " + this.getRateForBuyingCurrency(currencyName));
        } catch (CurrencyExchangeException e) {
            System.out.println(e.getMessage());
        }

        System.out.println();
        System.out.println("----------");
    }
}

