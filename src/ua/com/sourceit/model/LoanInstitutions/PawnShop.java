package ua.com.sourceit.model.LoanInstitutions;

import ua.com.sourceit.exceptions.CurrencyExchangeException;

public class PawnShop extends LoanInstitution {

    public PawnShop() {
    }

    public PawnShop(String name, String address, float maximumCreditCommissionPerYear, float maximumCreditSum) {
        super(name, address);

        super.setMaximumCreditCommissionPerYear(maximumCreditCommissionPerYear);
        super.setMaximumCreditSum(maximumCreditSum);
    }
}
