package ua.com.sourceit.model.LoanInstitutions;

public class CreditUnion extends LoanInstitution {

    public CreditUnion() {
    }

    public CreditUnion(String name, String address, float maximumCreditCommissionPerYear, float maximumCreditSum) {
        super(name, address);

        super.setMaximumCreditCommissionPerYear(maximumCreditCommissionPerYear);
        super.setMaximumCreditSum(maximumCreditSum);
    }
}
