package ua.com.sourceit.model.LoanInstitutions;

import ua.com.sourceit.interfaces.Credit;
import ua.com.sourceit.interfaces.CurrencyExchange;
import ua.com.sourceit.model.FinancialInstitution;
import ua.com.sourceit.utils.Helper;

public abstract class LoanInstitution extends FinancialInstitution implements Credit {

    // For Credit
    private float maximumCreditCommissionPerYear = 0;
    private float maximumCreditSum = 0;
    private String currencyName = "";

    //================================================================================//

    public LoanInstitution(){}

    public LoanInstitution(String name, String address){
        super(name, address);
    }

    //================================================================================//

    public float getMaximumCreditCommissionPerYear() {
        return maximumCreditCommissionPerYear;
    }

    public void setMaximumCreditCommissionPerYear(float maximumCreditCommissionPerYear) {
        this.maximumCreditCommissionPerYear = maximumCreditCommissionPerYear;
    }

    public float getMaximumCreditSum() {
        return maximumCreditSum;
    }

    public void setMaximumCreditSum(float maximumCreditSum) {
        this.maximumCreditSum = maximumCreditSum;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    //================================================================================//

    @Override
    public float calculateReturnCreditSum(String currencyName, float sum, int monthCount) {

        // Check sum > 0 and monthCount > 0

        /*


         */

        // Check Max/Min Credit Sum

        /*


         */

        // Check Max/Min month count for Credit

        /*


         */

        float returnCreditSum = 0;

        switch(currencyName) {

            case CurrencyExchange.UAH : {

                if (sum > maximumCreditSum)
                    System.out.println("Maximum credit sum = " + maximumCreditSum);
                else
                    returnCreditSum = (sum * maximumCreditCommissionPerYear / 100 / 12) * monthCount;

                break;
            }

            case CurrencyExchange.USD : {
                returnCreditSum = (sum * maximumCreditCommissionPerYear / 100 / 12) * monthCount;
                break;
            }

            case CurrencyExchange.EUR : {
                returnCreditSum = (sum * maximumCreditCommissionPerYear / 100 / 12) * monthCount;
            }
            default:
                break;
        }

        return Helper.roundUpToTwoDecimalPlaces(returnCreditSum);
    }

    //================================================================================//

    @Override
    public void printInfo(String currencyName){

//      System.out.println();
//      System.out.println("Financial Institution");
        System.out.println();
        System.out.println("Name: " + this.getName());
        System.out.println("Address: " + this.getAddress());

        System.out.println();
        System.out.println("Currency: " + currencyName);
        System.out.println();

        System.out.println("For Credit");
        System.out.println();
        System.out.println("Maximum Credit Sum: " + this.getMaximumCreditSum());
        System.out.println("Maximum Credit Commission per Year: " + this.getMaximumCreditCommissionPerYear());
        System.out.println();
        System.out.println("----------");
    }
}








