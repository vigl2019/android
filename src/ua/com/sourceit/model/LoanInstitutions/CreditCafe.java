package ua.com.sourceit.model.LoanInstitutions;

public class CreditCafe extends LoanInstitution {

    public CreditCafe() {
    }

    public CreditCafe(String name, String address, float maximumCreditCommissionPerYear, float maximumCreditSum) {
        super(name, address);

        super.setMaximumCreditCommissionPerYear(maximumCreditCommissionPerYear);
        super.setMaximumCreditSum(maximumCreditSum);
    }
}
