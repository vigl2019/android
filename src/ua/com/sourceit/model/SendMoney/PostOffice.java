package ua.com.sourceit.model.SendMoney;

import ua.com.sourceit.exceptions.CurrencyExchangeException;
import ua.com.sourceit.interfaces.PrintInfo;
import ua.com.sourceit.interfaces.SendMoney;
import ua.com.sourceit.model.FinancialInstitution;
import ua.com.sourceit.utils.Checker;
import ua.com.sourceit.utils.Helper;

public class PostOffice extends FinancialInstitution implements SendMoney, PrintInfo {

    private float commissionForMoneySending;

    public PostOffice(){}

    public PostOffice(String name, String address, float commissionForMoneySending){
        super(name, address);
        this.setCommissionForMoneySending(commissionForMoneySending);
    }

    //================================================================================//

    public float getCommissionForMoneySending() {
        return commissionForMoneySending;
    }

    public void setCommissionForMoneySending(float commissionForMoneySending) {
        this.commissionForMoneySending = commissionForMoneySending;
    }

    //================================================================================//

    @Override
    public float calculateMoneyForSend(String currencyName, float sum) throws CurrencyExchangeException {

        // Check sum
        Checker.checkCurrencyAmount(sum);

        float moneyForSend = (sum - (sum * (this.getCommissionForMoneySending() / 100)));

        return Helper.roundUpToTwoDecimalPlaces(moneyForSend);
    }

    //================================================================================//

    @Override
    public void printInfo(String currencyName){

//      System.out.println();
//      System.out.println("Financial Institution");
        System.out.println();
        System.out.println("Name: " + this.getName());
        System.out.println("Address: " + this.getAddress());

        System.out.println();
        System.out.println("For Money Sending");
        System.out.println();

        System.out.println("Currency: " + currencyName);
        System.out.println("Commission for Money Sending: " + this.getCommissionForMoneySending());
        System.out.println();
        System.out.println("----------");
    }
}


