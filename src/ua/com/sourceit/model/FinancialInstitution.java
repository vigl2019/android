package ua.com.sourceit.model;

import ua.com.sourceit.interfaces.PrintInfo;

// Base Class for all Financial Institutions
public abstract class FinancialInstitution implements PrintInfo {

    private String name = "";
    private String address = "";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    //================================================================================//

    public FinancialInstitution(){}

    public FinancialInstitution(String name, String address) {
        this.setName(name);
        this.setAddress(address);
    }
}


