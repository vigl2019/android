package ua.com.sourceit.model.DepositInstitutions;

import ua.com.sourceit.interfaces.CurrencyExchange;
import ua.com.sourceit.interfaces.Deposit;
import ua.com.sourceit.interfaces.PrintInfo;
import ua.com.sourceit.model.FinancialInstitution;
import ua.com.sourceit.utils.Helper;

public class PIF extends FinancialInstitution implements Deposit, PrintInfo {

    private int foundationYear;
    private float monthDepositPercent;
    private float maximumDepositSum;
    private int minimumMonthForDeposit;
    private String currencyName;

    public int getFoundationYear() {
        return foundationYear;
    }

    public void setFoundationYear(int foundationYear) {
        this.foundationYear = foundationYear;
    }

    public float getMonthDepositPercent() {
        return monthDepositPercent;
    }

    public void setMonthDepositPercent(float monthDepositPercent) {
        this.monthDepositPercent = monthDepositPercent;
    }

    public float getMaximumDepositSum() {
        return maximumDepositSum;
    }

    public void setMaximumDepositSum(float maximumDepositSum) {
        this.maximumDepositSum = maximumDepositSum;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public int getMinimumMonthForDeposit() {
        return minimumMonthForDeposit;
    }

    public void setMinimumMonthForDeposit(int minimumMonthForDeposit) {
        this.minimumMonthForDeposit = minimumMonthForDeposit;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    //================================================================================//

    public PIF() {
    }

    public PIF(String name, String address, int foundationYear, float monthDepositPercent, float maximumDepositSum, int minimumMonthForDeposit) {
        super(name, address);

        this.setFoundationYear(foundationYear);
        this.setMonthDepositPercent(monthDepositPercent);
        this.setMaximumDepositSum(maximumDepositSum);
        this.setMinimumMonthForDeposit(minimumMonthForDeposit);
    }

    //================================================================================//

    @Override
    public float calculateReturnDepositSum(String currencyName, float sum, int monthCount) {

        // Check sum > 0 and monthCount > 0

        /*


         */

        // Check Max/Min Deposit Sum

        /*


         */

        // Check Max/Min month count for Deposit

        /*


         */

        float returnDepositSum = 0;

        switch(currencyName) {

            case CurrencyExchange.UAH : {

                if (sum > maximumDepositSum)
                    System.out.println("Maximum deposit sum = " + maximumDepositSum);
                else
                    returnDepositSum = sum * monthCount * (monthDepositPercent / 100);

                break;
            }

            case CurrencyExchange.USD : {
                returnDepositSum = sum * monthCount * (monthDepositPercent / 100);
                break;
            }

            case CurrencyExchange.EUR : {
                returnDepositSum = sum * monthCount * (monthDepositPercent / 100);
            }
            default:
                break;
        }

        return Helper.roundUpToTwoDecimalPlaces(returnDepositSum);
    }

    //================================================================================//

    @Override
    public void printInfo(String currencyName){

//      System.out.println();
//      System.out.println("Financial Institution");
        System.out.println();
        System.out.println("Name: " + this.getName());
        System.out.println("Address: " + this.getAddress());
        System.out.println("Foundation Year: " + this.getFoundationYear());

        System.out.println();
        System.out.println("For Deposit");
        System.out.println();

        System.out.println("Currency: " + currencyName);
        System.out.println("Maximum Deposit Sum: " + this.getMaximumDepositSum());
        System.out.println("Month Deposit Percent: " + this.getMonthDepositPercent());
        System.out.println("Minimum Months for Deposit: " + this.getMinimumMonthForDeposit());
        System.out.println();
        System.out.println("----------");
    }
}



