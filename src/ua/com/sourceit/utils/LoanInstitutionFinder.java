package ua.com.sourceit.utils;

import ua.com.sourceit.interfaces.Credit;
import ua.com.sourceit.model.FinancialInstitution;

import java.util.List;

public class LoanInstitutionFinder {
    public LoanInstitutionFinder() {
    }

    //================================================================================//

    public static void getReturnMinimumCreditSum(List<FinancialInstitution> financialInstitutions, String currencyName, float sum, int monthCount) {

        // Check sum > 0 and monthCount > 0

        /*



         */

        // Find the best Loan Institution

        float returnMinimumCreditSum = 1_000_000_000;

        FinancialInstitution bestFinancialInstitution = null;

        for (int i = 0; i < financialInstitutions.size(); i++) {
            Credit credit = (Credit) financialInstitutions.get(i);
            float returnCreditSum = credit.calculateReturnCreditSum(currencyName, sum, monthCount);

            if (returnCreditSum < returnMinimumCreditSum) {
                returnMinimumCreditSum = returnCreditSum;
                bestFinancialInstitution = financialInstitutions.get(i);
            }

/*
            //==================================================//

            // For testing
            System.out.println();
            System.out.println("-- For testing --");
            System.out.println();
            System.out.println("Financial Institution: " + financialInstitutions.get(i).getName());
            System.out.println("Return Minimum Credit Sum: " + returnCreditSum);
            System.out.println("Sum for loan: " + sum);
            System.out.println("Month Count: " + monthCount);
            System.out.println();
            System.out.println("-- For testing --");
            System.out.println();

            //==================================================//
*/
        }

        System.out.println();
        System.out.println("The best Financial Institution for Loan money");

        // Print info about the best Loan Institution
        bestFinancialInstitution.printInfo(currencyName);

        // Print info about financial transaction
        System.out.println();
        System.out.println("Return Minimum Credit Sum: " + returnMinimumCreditSum);
        System.out.println("Sum for loan: " + sum);
        System.out.println("Month Count: " + monthCount);
        System.out.println();
        System.out.println("//==================================================//");
        System.out.println();
    }
}


