package ua.com.sourceit.utils;

import ua.com.sourceit.exceptions.CurrencyExchangeException;
import ua.com.sourceit.interfaces.SendMoney;
import ua.com.sourceit.model.FinancialInstitution;

import java.util.List;

public class SendingMoneyFinder {
    public SendingMoneyFinder() {
    }

    //================================================================================//

    public static void getSumForSendMoney(List<FinancialInstitution> financialInstitutions, String currencyName, float sum) {

        // Check sum > 0

        /*



         */

        // Find the best Sending Money Institution

        float sumForSendMoney = 0.00f;
        float maxSumForSendMoney = 0.00f;

        FinancialInstitution bestFinancialInstitution = null;

        for (int i = 0; i < financialInstitutions.size(); i++) {

            SendMoney sendMoney = (SendMoney) financialInstitutions.get(i);

            try {
                sumForSendMoney = sendMoney.calculateMoneyForSend(currencyName, sum);
            } catch (CurrencyExchangeException e) {
                System.out.println(e.getMessage());
            }

            if (sumForSendMoney > maxSumForSendMoney) {
                maxSumForSendMoney = sumForSendMoney;
                bestFinancialInstitution = financialInstitutions.get(i);
            }

/*
            //==================================================//

            // For testing
            System.out.println();
            System.out.println("-- For testing --");
            System.out.println();
            System.out.println("Financial Institution: " + financialInstitutions.get(i).getName());
            System.out.println("Maximum Sending Sum: " + sumForSendMoney);
            System.out.println("Sum for Send Money before calculate: " + sum);
            System.out.println();
            System.out.println("-- For testing --");
            System.out.println();

            //==================================================//
*/
        }

        System.out.println();
        System.out.println("The best Financial Institution for Send Money");

        // Print info about the best Sending Money Institution
        bestFinancialInstitution.printInfo(currencyName);

        // Print info about financial transaction
        System.out.println();
        System.out.println("Maximum Sending Sum: " + maxSumForSendMoney);
        System.out.println("Sum for Send Money before calculate: " + sum);
        System.out.println();
        System.out.println("//==================================================//");
        System.out.println();
    }
}



