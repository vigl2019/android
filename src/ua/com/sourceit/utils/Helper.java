package ua.com.sourceit.utils;

import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;

public class Helper {

    private static String mainProperties = "./data/resources/main.properties";

    public static float convertDoubleToFloat(Double value) {

        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        value = Double.valueOf(decimalFormat.format(value));

        return Float.parseFloat(value.toString());
    }

    public static float roundUpToTwoDecimalPlaces(float value) {
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        value = Float.valueOf(decimalFormat.format(value));

        return value;
    }

    public static String getProperty(String propertyName) {
        Properties properties = new Properties();
        try {
            properties.load(new FileReader(mainProperties));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties.getProperty(propertyName);
    }
}
