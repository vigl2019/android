package ua.com.sourceit.utils;

import ua.com.sourceit.exceptions.CurrencyExchangeException;

public class Checker {

    public Checker() {
    }

    //================================================================================//

    public static void checkCurrencyAmount(float currencyAmount) throws CurrencyExchangeException {
        if (currencyAmount == 0)
            throw new CurrencyExchangeException("Currency = 0");

        if (currencyAmount < 0)
            throw new CurrencyExchangeException("Currency < 0");
    }

    public static void checkRate(float rate) throws CurrencyExchangeException {

        if (rate == 0)
            throw new CurrencyExchangeException("Rate = 0");

        if (rate < 0)
            throw new CurrencyExchangeException("Rate < 0");
    }
}


