package ua.com.sourceit.utils;

import java.util.List;

import ua.com.sourceit.interfaces.CurrencyExchange;
import ua.com.sourceit.exceptions.CurrencyExchangeException;
import ua.com.sourceit.model.CurrencyInstitutions.CurrencyInstitution;

public class CurrencyBuyerAndSeller {

    // Buy currency
    public static void buyCurrencyAtTheBestRate(List<CurrencyInstitution> currencyInstitutions, float currencyAmount, String sellingCurrencyName, String buyingCurrencyName) {

        // Find the best rate for buying currency
        CurrencyInstitution currencyInstitution = getCurrencyInstitutionWithBestRateForBuyingCurrency(buyingCurrencyName, currencyInstitutions);

        if (currencyInstitution != null) {

            // Print info about Financial Institution
//          PrintInfo.printBuyingInfoAboutCurrencyInstitution(buyingCurrencyName, currencyInstitution);

            System.out.println();
            System.out.println("The best Financial Institution for buying currency");
            currencyInstitution.printInfo(buyingCurrencyName);
            System.out.println();

//          CurrencyExchange currencyExchange = (CurrencyExchange) currencyInstitution;
            float amountBuyingCurrency = 0;

            // Buy currency
            try {
//              amountBuyingCurrency = currencyExchange.buyCurrency(currencyAmount, buyingCurrencyName);
                amountBuyingCurrency = currencyInstitution.buyCurrency(currencyAmount, buyingCurrencyName);
            } catch (CurrencyExchangeException e) {
                System.out.println(e.getMessage());
            }

            // Print info about buying currency operation
            System.out.println(CurrencyExchange.SELL + ": " + currencyAmount + " " + sellingCurrencyName);
            System.out.println("Commission Percent: " + currencyInstitution.getCommissionPercent());
            System.out.println(CurrencyExchange.BUY + ": " + Helper.roundUpToTwoDecimalPlaces(amountBuyingCurrency) + " " + buyingCurrencyName);
            System.out.println();
            System.out.println("//==================================================//");
        }
    }

    // Sell currency
    public static void sellCurrencyAtTheBestRate(List<CurrencyInstitution> currencyInstitutions, float currencyAmount, String sellingCurrencyName, String buyingCurrencyName) {

        // Find the best rate for selling currency
        CurrencyInstitution currencyInstitution = getCurrencyInstitutionWithBestRateForSellingCurrency(sellingCurrencyName, currencyInstitutions);

        if (currencyInstitution != null) {

            // Print info about Financial Institution
//          PrintInfo.printSellingInfoAboutCurrencyInstitution(sellingCurrencyName, currencyInstitution);

            System.out.println();
            System.out.println("The best Financial Institution for selling currency");
            currencyInstitution.printInfo(sellingCurrencyName);
            System.out.println();

//          CurrencyExchange currencyExchange = (CurrencyExchange) currencyInstitution;
            float amountBuyingCurrency = 0;

            // Sell currency
            try {
//              amountBuyingCurrency = currencyExchange.sellCurrency(currencyAmount, sellingCurrencyName);
                amountBuyingCurrency = currencyInstitution.sellCurrency(currencyAmount, sellingCurrencyName);
            } catch (CurrencyExchangeException e) {
                System.out.println(e.getMessage());
            }

            // Print info about selling currency operation
            System.out.println(CurrencyExchange.SELL + ": " + currencyAmount + " " + sellingCurrencyName);
            System.out.println("Commission Percent: " + currencyInstitution.getCommissionPercent());
            System.out.println(CurrencyExchange.BUY + ": " + Helper.roundUpToTwoDecimalPlaces(amountBuyingCurrency) + " " + buyingCurrencyName);
            System.out.println();
            System.out.println("//==================================================//");
        }
    }

    // Check the best exchange rate for buying currency
    public static CurrencyInstitution getCurrencyInstitutionWithBestRateForBuyingCurrency(String currencyName, List<CurrencyInstitution> currencyInstitutions) {

        float bestRateBuy = 1_000_000.0f;
        CurrencyInstitution currencyInstitutionWithBestRateForBuyingCurrency = null;

        for (int i = 0; i < currencyInstitutions.size(); i++) {

            CurrencyInstitution currencyInstitution = currencyInstitutions.get(i);
            float instanceBestRate = 0;

            try {
                instanceBestRate = currencyInstitution.getRateForBuyingCurrency(currencyName);
            }
            catch (CurrencyExchangeException e){
                System.out.println(e.getMessage());
            }

            if (instanceBestRate < bestRateBuy) {
                bestRateBuy = instanceBestRate;
                currencyInstitutionWithBestRateForBuyingCurrency = currencyInstitution;
            }
        }

        return currencyInstitutionWithBestRateForBuyingCurrency;
    }

    // Check the best exchange rate for selling currency
    public static CurrencyInstitution getCurrencyInstitutionWithBestRateForSellingCurrency(String currencyName, List<CurrencyInstitution> currencyInstitutions) {

        float bestRateSell = 0.0f;
        CurrencyInstitution currencyInstitutionWithBestRateForSellingCurrency = null;

        for (int i = 0; i < currencyInstitutions.size(); i++) {

            CurrencyInstitution currencyInstitution = currencyInstitutions.get(i);
            float instanceBestRate = 0;

            try {
                instanceBestRate = currencyInstitution.getRateForSellingCurrency(currencyName);
            }
            catch (CurrencyExchangeException e){
                System.out.println(e.getMessage());
            }

            if (instanceBestRate > bestRateSell) {
                bestRateSell = instanceBestRate;
                currencyInstitutionWithBestRateForSellingCurrency = currencyInstitution;
            }
        }

        return currencyInstitutionWithBestRateForSellingCurrency;
    }
}

