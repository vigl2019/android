package ua.com.sourceit.utils;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import ua.com.sourceit.interfaces.CurrencyExchange;
import ua.com.sourceit.model.CurrencyInstitutions.Bank;
import ua.com.sourceit.model.CurrencyInstitutions.ExchangeOffice;
import ua.com.sourceit.model.DepositInstitutions.PIF;
import ua.com.sourceit.model.LoanInstitutions.CreditCafe;
import ua.com.sourceit.model.LoanInstitutions.CreditUnion;
import ua.com.sourceit.model.LoanInstitutions.PawnShop;
import ua.com.sourceit.model.SendMoney.PostOffice;

import static ua.com.sourceit.utils.Helper.convertDoubleToFloat;
import static ua.com.sourceit.utils.Helper.getProperty;

public class Generator {

    public static Bank generateBank() {
        Bank bank = new Bank(generateName("Bank"), generateAddress("BankAddress"), generateYear(), generateCommissionPercent());

        float commissionFixedAmount = Float.parseFloat(getProperty("commission.fixed.amount"));
        bank.setCommissionFixedAmount(commissionFixedAmount);

        float maximumExchangeAmountUAH = Float.parseFloat(getProperty("maximum.exchange.amount.UAH"));
        bank.setMaximumExchangeAmount(maximumExchangeAmountUAH);

        bank.setMonthDepositPercent(generateMonthDepositPercentForBank());
        bank.setMaximumDepositSum(generateMaximumDepositSumForBank());
        bank.setMaximumMonthForDeposit(generateMaximumMonthForDepositForBank());
        bank.setCommissionForMoneySending(generateCommissionForMoneySendingForBank());
        bank.setCommissionFixedAmountForMoneySending(generateCommissionFixedAmountForMoneySendingForBank());

        return bank;
    }

    public static ExchangeOffice generateExchangeOffice() {
        ExchangeOffice exchangeOffice = new ExchangeOffice(generateName("ExchangeOffice"), generateAddress("ExchangeOfficeAddress"), generateCommissionPercent());
        return exchangeOffice;
    }

    public static PawnShop generatePawnShop(){
        PawnShop pawnShop = new PawnShop(generateName("PawnShop"), generateAddress("PawnShopAddress"), getMaximumCreditCommissionPerYearForPawnShop(), getMaximumCreditSumForPawnShop());
        return pawnShop;
    }

    public static CreditCafe generateCreditCafe(){
        CreditCafe creditCafe = new CreditCafe(generateName("CreditCafe"), generateAddress("CreditCafeAddress"), getMaximumCreditCommissionPerYearForCreditCafe(), getMaximumCreditSumForCreditCafe());
        return creditCafe;
    }

    public static CreditUnion generateCreditUnion(){
        CreditUnion creditUnion = new CreditUnion(generateName("CreditUnion"), generateAddress("CreditUnionAddress"), getMaximumCreditCommissionPerYearForCreditUnion(), getMaximumCreditSumForCreditUnion());
        return creditUnion;
    }

    public static PIF generatePIF(){
        PIF pif = new PIF(generateName("PIF"), generateAddress("PIFaddress"), generateYear(), generateMonthDepositPercentForPIF(), generateMaximumDepositSumForPIF(), generateMinimumMonthForDepositForPIF());
        return pif;
    }

    public static PostOffice generatePostOffice(){
        PostOffice postOffice = new PostOffice(generateName("PostOffice"), generateAddress("PostOfficeAddress"), generateCommissionForMoneySendingForPostOffice());
        return postOffice;
    }

    //================================================================================//

    private static String generateName(String name) {
        Random random = new Random();
        return name + " # " + String.valueOf(random.nextInt(1_000) + 5);
    }

    private static String generateAddress(String address) {
        Random random = new Random();
        return "Address " + address + " # " + String.valueOf(random.nextInt(1_000) + 5);
    }

    private static float generateCommissionPercent() {
        return convertDoubleToFloat(Math.random() + 3);
    }

    public static float generateCurrencyAmount() {

        DecimalFormat decimalFormat = new DecimalFormat("#.");
        Double value = Double.valueOf(decimalFormat.format(Math.random() * 10_000 + 10_000));

        return Float.parseFloat(value.toString());
    }

    public static float generateRate(String currencyName) {
        Random random = new Random();
        double rate = 1;

        switch (currencyName.trim()) {
            case CurrencyExchange.USD: {
                rate = Math.random() + Float.parseFloat(Helper.getProperty("base.rate.USD"));
                break;
            }
            case CurrencyExchange.EUR: {
                rate = Math.random() + Float.parseFloat(Helper.getProperty("base.rate.EUR"));
                break;
            }
            case CurrencyExchange.RUB: {
                rate = Math.abs((Math.random() * 2) - 1.6);
                break;
            }
            default:
                break;
        }

/*
        Locale locale = Locale.ENGLISH;
        NumberFormat numberFormat = NumberFormat.getNumberInstance(locale);
        numberFormat.setMinimumFractionDigits(2); // trailing zeros
        numberFormat.setMaximumFractionDigits(2); // round to 2 digits
        numberFormat.format(rate);
*/

        return convertDoubleToFloat(rate);
    }

    private static int generateYear() {
        Random random = new Random();
        return 1900 + random.nextInt(105);
    }

    //================================================================================//

    public static Map<String, Map<String, Float>> generateCurrencies(String... currenciesName) {

        Map<String, Map<String, Float>> currencies = new HashMap<>();

        for (String currencyName : currenciesName) {

            currencies.put(currencyName, new HashMap<>(2, 1.1f));

            float buyCurrency = Generator.generateRate(currencyName);
            currencies.get(currencyName).put(CurrencyExchange.BUY, buyCurrency);

            float sellCurrency = Generator.generateRate(currencyName);
            while (buyCurrency <= sellCurrency)
                sellCurrency = Generator.generateRate(currencyName);

            currencies.get(currencyName).put(CurrencyExchange.SELL, sellCurrency);
        }

        return currencies;
    }

    public static String generateCurrenciesName(){
        String[] currenciesName = new String[]{CurrencyExchange.USD, CurrencyExchange.EUR};
        Random random = new Random();

        return currenciesName[random.nextInt(2)];
    }

    // For Bank

    public static float generateMonthDepositPercentForBank(){
        float monthDepositPercent = 0;
        String monthDepositPercentString = Helper.getProperty("bank.monthDepositPercent");
        monthDepositPercent = Float.parseFloat(monthDepositPercentString) + Helper.convertDoubleToFloat(Math.random() + 1);
        return Helper.roundUpToTwoDecimalPlaces(monthDepositPercent);
    }

    public static float generateMaximumDepositSumForBank(){
        float maximumDepositSum = 0;
        String maximumDepositSumString = Helper.getProperty("bank.maximumDepositSum");
        maximumDepositSum = Float.parseFloat(maximumDepositSumString);
        return Helper.roundUpToTwoDecimalPlaces(maximumDepositSum);
    }

    public static int generateMaximumMonthForDepositForBank(){
        String minimumMonthForDeposit = Helper.getProperty("bank.maximumMonthForDeposit");
        return Integer.parseInt(minimumMonthForDeposit);
    }

    //================================================================================//

    // For Loan Institution

    private static float getMaximumCreditCommissionPerYearForPawnShop(){
        float maximumCreditCommissionPerYear = 0;
        String maximumCreditCommissionPerYearString = Helper.getProperty("PawnShop.maximumCreditCommissionPerYear");
        maximumCreditCommissionPerYear = Float.parseFloat(maximumCreditCommissionPerYearString) + Helper.convertDoubleToFloat(Math.random() + 1);
        return Helper.roundUpToTwoDecimalPlaces(maximumCreditCommissionPerYear);
    }

    private static float getMaximumCreditSumForPawnShop(){
        float maximumCreditSum = 0;
        String maximumCreditSumString = Helper.getProperty("PawnShop.maximumCreditSum");
        maximumCreditSum = Float.parseFloat(maximumCreditSumString);
        return Helper.roundUpToTwoDecimalPlaces(maximumCreditSum);
    }

    private static float getMaximumCreditCommissionPerYearForCreditCafe(){
        float maximumCreditCommissionPerYear = 0;
        String maximumCreditCommissionPerYearString = Helper.getProperty("CreditCafe.maximumCreditCommissionPerYear");
        maximumCreditCommissionPerYear = Float.parseFloat(maximumCreditCommissionPerYearString) + Helper.convertDoubleToFloat(Math.random() + 1);
        return Helper.roundUpToTwoDecimalPlaces(maximumCreditCommissionPerYear);
    }

    private static float getMaximumCreditSumForCreditCafe(){
        float maximumCreditSum = 0;
        String maximumCreditSumString = Helper.getProperty("CreditCafe.maximumCreditSum");
        maximumCreditSum = Float.parseFloat(maximumCreditSumString);
        return Helper.roundUpToTwoDecimalPlaces(maximumCreditSum);
    }

    private static float getMaximumCreditCommissionPerYearForCreditUnion(){
        float maximumCreditCommissionPerYear = 0;
        String maximumCreditCommissionPerYearString = Helper.getProperty("CreditUnion.maximumCreditCommissionPerYear");
        maximumCreditCommissionPerYear = Float.parseFloat(maximumCreditCommissionPerYearString) + Helper.convertDoubleToFloat(Math.random() + 1);
        return Helper.roundUpToTwoDecimalPlaces(maximumCreditCommissionPerYear);
    }

    private static float getMaximumCreditSumForCreditUnion(){
        float maximumCreditSum = 0;
        String maximumCreditSumString = Helper.getProperty("CreditUnion.maximumCreditSum");
        maximumCreditSum = Float.parseFloat(maximumCreditSumString);
        return Helper.roundUpToTwoDecimalPlaces(maximumCreditSum);
    }

    //================================================================================//

    // For Loan Institution

    public static float generateSumForLoan(){

        DecimalFormat decimalFormat = new DecimalFormat("#.");
//      Double value = Double.valueOf(decimalFormat.format(Math.random() * 50_000 + 10_000));
        Double value = Double.valueOf(decimalFormat.format(Math.random() * 4_000 + 1_000));

        return Float.parseFloat(value.toString());
    }

    public static int generateMonthCount(){
        Random random = new Random();
        return 12 + random.nextInt(25);
    }

    //================================================================================//

    // For Deposit

    public static float generateMonthDepositPercentForPIF(){
        float monthDepositPercent = 0;
        String monthDepositPercentString = Helper.getProperty("PIF.monthDepositPercent");
        monthDepositPercent = Float.parseFloat(monthDepositPercentString) + Helper.convertDoubleToFloat(Math.random() + 1);
        return Helper.roundUpToTwoDecimalPlaces(monthDepositPercent);
    }

    public static float generateMaximumDepositSumForPIF(){
        float maximumDepositSum = 0;
        String maximumDepositSumString = Helper.getProperty("PIF.maximumDepositSum");
        maximumDepositSum = Float.parseFloat(maximumDepositSumString);
        return Helper.roundUpToTwoDecimalPlaces(maximumDepositSum);
    }

    public static int generateMinimumMonthForDepositForPIF(){
        String minimumMonthForDeposit = Helper.getProperty("PIF.minimumMonthForDeposit");
        return Integer.parseInt(minimumMonthForDeposit);
    }

    public static float generateSumForDeposit(){

        DecimalFormat decimalFormat = new DecimalFormat("#.");
        Double value = Double.valueOf(decimalFormat.format(Math.random() * 80_000 + 1_000));

        return Float.parseFloat(value.toString());
    }

    //================================================================================//

    // For Money Sending

    public static float generateCommissionForMoneySendingForBank(){
        float commissionForMoneySending = 0;
        Random random = new Random();
        double commissionForMoneySendingDouble = Math.random() + Float.parseFloat(Helper.getProperty("bank.commissionForMoneySending"));
        commissionForMoneySending = Helper.convertDoubleToFloat(commissionForMoneySendingDouble);

        return Helper.roundUpToTwoDecimalPlaces(commissionForMoneySending);
    }

    public static float generateCommissionFixedAmountForMoneySendingForBank(){
        float commissionFixedAmountForMoneySending = 0;
        Random random = new Random();
        double commissionFixedAmountForMoneySendingDouble = Math.random() + Float.parseFloat(Helper.getProperty("bank.commissionFixedAmountForMoneySending"));
        commissionFixedAmountForMoneySending = Helper.convertDoubleToFloat(commissionFixedAmountForMoneySendingDouble);

        return Helper.roundUpToTwoDecimalPlaces(commissionFixedAmountForMoneySending);
    }

    public static float generateCommissionForMoneySendingForPostOffice(){
        float commissionForMoneySending = 0;
        Random random = new Random();
        double commissionForMoneySendingDouble = Math.random() + Float.parseFloat(Helper.getProperty("PostOffice.commissionForMoneySending"));
        commissionForMoneySending = Helper.convertDoubleToFloat(commissionForMoneySendingDouble);

        return Helper.roundUpToTwoDecimalPlaces(commissionForMoneySending);
    }

    public static float generateSumForSendMoney(){

        DecimalFormat decimalFormat = new DecimalFormat("#.");
        Double value = Double.valueOf(decimalFormat.format(Math.random() * 50_000 + 1_000));

        return Float.parseFloat(value.toString());
    }
}

