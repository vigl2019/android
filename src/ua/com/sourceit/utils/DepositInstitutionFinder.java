package ua.com.sourceit.utils;

import ua.com.sourceit.interfaces.Deposit;
import ua.com.sourceit.model.FinancialInstitution;

import java.util.List;

public class DepositInstitutionFinder {

    public DepositInstitutionFinder() {
    }

    //================================================================================//

    public static void getReturnMaximumDepositSum(List<FinancialInstitution> financialInstitutions, String currencyName, float sum, int monthCount) {

        // Check sum > 0 and monthCount > 0

        /*



         */

        // Find the best Deposit Institution

        float returnMaximumDepositSum = 0.00f;

        FinancialInstitution bestFinancialInstitution = null;

        for (int i = 0; i < financialInstitutions.size(); i++) {
            Deposit deposit = (Deposit) financialInstitutions.get(i);
            float returnDepositSum = deposit.calculateReturnDepositSum(currencyName, sum, monthCount);

            if (returnDepositSum > returnMaximumDepositSum) {
                returnMaximumDepositSum = returnDepositSum;
                bestFinancialInstitution = financialInstitutions.get(i);
            }
/*

            //==================================================//

            // For testing
            System.out.println();
            System.out.println("-- For testing --");
            System.out.println();
            System.out.println("Financial Institution: " + financialInstitutions.get(i).getName());
            System.out.println("Return Maximum Deposit Sum: " + returnDepositSum);
            System.out.println("Sum for Deposit: " + sum);
            System.out.println("Month Count: " + monthCount);
            System.out.println();
            System.out.println("-- For testing --");
            System.out.println();

            //==================================================//
*/
        }

        System.out.println();
        System.out.println("The best Financial Institution for Deposit money");

        // Print info about the best Deposit Institution
        bestFinancialInstitution.printInfo(currencyName);

        // Print info about financial transaction
        System.out.println();
        System.out.println("Return Maximum Deposit Sum: " + returnMaximumDepositSum);
        System.out.println("Sum for Deposit: " + sum);
        System.out.println("Month Count: " + monthCount);
        System.out.println();
        System.out.println("//==================================================//");
        System.out.println();
    }
}
